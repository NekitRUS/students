// program.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include "windows.h"
#include <iostream>
#include <locale>
#include <string>
#include <algorithm>
#include <fstream>
#include <vector>

using namespace std;

const char path[] = "E:\\�����\\����������������\\��������\\DB_Students.txt";


struct student
{
	char familiya[25];
	char name[25];
	int day;
	int month;
	int year;
	char institute[25];
	char team[15];
	int course;
	float sr_ball;
};




//���������, ��������� �� ����� ������, ���������� ��������� "�������"
void show(vector<student>& students_list)
{
	cout << "�\t    ���\t\t  ���� ��������\t  ��������   ������  ����  ������� ����" << endl;
	for(int i = 0; i < students_list.size(); i++)
	{
		printf("%1d %9s %8s", (i + 1), students_list[i].familiya, students_list[i].name);
		printf("\t   %2d.%2d.%4d", students_list[i].day, students_list[i].month, students_list[i].year);
		printf("  %9s %10s   %1d\t    %7.2f\n", students_list[i].institute, students_list[i].team, students_list[i].course, students_list[i].sr_ball);
	}
	cout << "____________________________________________________________________________" << endl << endl;
}




//�������, ����������� ��������� "�������" �� ����� ��������� � ����������� ��� ������
vector<student> read()
{
	FILE * pFile;
	pFile = fopen(path, "r");
	vector<student> students_list;
	while (!feof(pFile))
	{
		student temp;
		fscanf(pFile, "%s %s %d %d %d %s %s %d %f\n", temp.familiya, temp.name, &temp.day, &temp.month, &temp.year, temp.institute, temp.team, &temp.course, &temp.sr_ball);
		students_list.push_back(temp);
	}
	fclose(pFile);
	return students_list;
}




//���������, ������������ � ���� ��������� "�������" �� �������
void write (vector<student>& students_list)
{
	if (students_list.empty())
	{
		cout << "������ �������� ������ ���� ������, �������� ���� �� ���� ������" << endl;
		system("pause");
		return;
	}
	FILE * pfile;
	pfile = fopen(path, "w");
	for (int i = 0; i < students_list.size(); i++)
	{
		fprintf(pfile, "%s %s %d %d %d %s %s %d %f\n", students_list[i].familiya, students_list[i].name, students_list[i].day, students_list[i].month, students_list[i].year, students_list[i].institute, students_list[i].team, students_list[i].course, students_list[i].sr_ball);
	}
	fclose(pfile);
}



//��������� ����������� �������� "�������" �� ���� ��������
bool birthday_predicate (const student& s1, const student& s2)
{
	if ((s1.year == s2.year) && (s1.month == s2.month))
		return s1.day > s2.day;
	if (s1.year == s2.year)
		return s1.month > s2.month;
	return s1.year > s2.year;
}




//��������� ����������� �������� "�������" �� ���
bool fio_predicate (const student& s1, const student& s2)
{
	if (!strcmp(s1.familiya, s2.familiya))
	{
		if (strcmp(s1.name, s2.name) <= 0)
			return true;
		else
			return  false;
	}
	if (strcmp(s1.familiya, s2.familiya) <= 0)
		return true;
	else
		return  false;
}



//��������� ���������� �� ���� ��������
void sort_birthday (vector<student>& students_list)
{
	sort(students_list.begin(), students_list.end(), birthday_predicate);
}




//��������� ���������� �� ���
void sort_fio (vector<student>& students_list)
{
	sort(students_list.begin(), students_list.end(), fio_predicate);
}



//���������, ������������� � ��������� �� ����� min, max, ������� � ����� ���� "������� ����" ��������� "�������"
void balli (vector<student>& students_list)
{
	if (students_list.empty())
	{
		cout << "���� ������ �����" << endl;
		system("pause");
		return;
	}
	float max = students_list[0].sr_ball;
	float min = students_list[0].sr_ball;
	float average = 0.0f;
	float sum = 0.0f;
	int imax = 0;;
	int imin = 0;
	for (int i = 0; i < students_list.size(); i++)
	{
		sum += students_list[i].sr_ball;
		if (students_list[i].sr_ball > max)
		{
			max = students_list[i].sr_ball;
			imax = i;
		}
		if (students_list[i].sr_ball < min)
		{
			min = students_list[i].sr_ball;
			imin = i;
		}
	}
	average = sum / students_list.size();
	printf("������������ ������� ���� ����� %5.2f � %s %s\n", max, students_list[imax].familiya, students_list[imax].name);
	printf("����������� ������� ���� ����� %5.2f � %s %s\n", min, students_list[imin].familiya, students_list[imin].name);
	printf("������� ���� ����� %5.2f\n", average);
	printf("����� ������� ������ ����� %5.2f\n", sum);
	system("pause");
}




//��������� ������ � ������� �������� "�������" �� ���
void find_fio (vector<student>& students_list)
{
	if (students_list.empty())
	{
		cout << "���� ������ �����" << endl;
		system("pause");
		return;
	}
	char familiya[50];
	char name[50];
	bool flag = false;
	cout << "������� �������" << endl;
	SetConsoleCP(1251);
	cin >> familiya;
	fflush (stdin);
	SetConsoleCP(866);
	cout << "������� ���" << endl;
	SetConsoleCP(1251);
	cin >> name;
	fflush (stdin);
	SetConsoleCP(866);
	for (int i = 0; i < students_list.size(); i++)
	{
		if ((!strcmp(familiya, students_list[i].familiya)) && (!strcmp(name, students_list[i].name)))
		{
			flag = true;
			printf("%1d %9s %8s", (i + 1), students_list[i].familiya, students_list[i].name);
			printf("\t   %2d.%2d.%4d", students_list[i].day, students_list[i].month, students_list[i].year);
			printf("  %9s %10s   %1d\t    %7.2f\n", students_list[i].institute, students_list[i].team, students_list[i].course, students_list[i].sr_ball);
		}
	}
	if (!flag)
		cout << "����� �� ��� �����������..." << endl;
	system("pause");
}




//��������� ������ � ������� �������� "�������" �� ���� ��������
void find_birthday(vector<student>& students_list)
{
	if (students_list.empty())
	{
		cout << "���� ������ �����" << endl;
		system("pause");
		return;
	}
	int day, month, year;
	bool flag = false;
	while (true)
	{
		cout << "������� ����" << endl;
		int temp = scanf("%d", &day);
		fflush (stdin);
		if (!temp)
			continue;
		cout << "������� �����" << endl;
		temp = scanf("%d", &month);
		fflush (stdin);
		if (!temp)
			continue;
		cout << "������� ���" << endl;
		temp = scanf("%d", &year);
		fflush (stdin);
		if (!temp)
			continue;
		break;
	}
	for (int i = 0; i < students_list.size(); i++)
	{
		if ((students_list[i].day == day) && (students_list[i].month == month) && (students_list[i].year == year))
		{
			flag = true;
			printf("%1d %9s %8s", (i + 1), students_list[i].familiya, students_list[i].name);
			printf("\t   %2d.%2d.%4d", students_list[i].day, students_list[i].month, students_list[i].year);
			printf("  %9s %10s   %1d\t    %7.2f\n", students_list[i].institute, students_list[i].team, students_list[i].course, students_list[i].sr_ball);
		}
	}
	if (!flag)
		cout << "����� �� ��� �����������..." << endl;
	system("pause");
}


//��������� ���������� ��������� "�������" � ������
void add_element(vector<student>& students_list)
{
	student temp;
	while (true)
	{
		cout << "������� �������" << endl;
		SetConsoleCP(1251);
		scanf("%s", temp.familiya);
		SetConsoleCP(866);
		cout << "������� ���" << endl;
		SetConsoleCP(1251);
		scanf("%s", temp.name);
		SetConsoleCP(866);
		cout << "������� ����" << endl;
		int flag = scanf("%d", &temp.day);
		fflush (stdin);
		if (!flag)
			continue;
		cout << "������� �����" << endl;
		flag = scanf("%d", &temp.month);
		fflush (stdin);
		if (!flag)
			continue;
		cout << "������� ���" << endl;
		flag = scanf("%d", &temp.year);
		fflush (stdin);
		if (!flag)
			continue;
		cout << "������� ��������" << endl;
		SetConsoleCP(1251);
		scanf("%s", temp.institute);
		SetConsoleCP(866);
		cout << "������� ������" << endl;
		SetConsoleCP(1251);
		scanf("%s", temp.team);
		SetConsoleCP(866);
		cout << "������� ����" << endl;
		flag = scanf("%d", &temp.course);
		fflush (stdin);
		if (!flag)
			continue;
		cout << "������� �����" << endl;
		flag = scanf("%f", &temp.sr_ball);
		fflush (stdin);
		if (!flag)
			continue;
		break;
	}
	students_list.push_back(temp);
}


//��������� ��������� ��������� "�������" � �������
void edit_element (vector<student>& students_list)
{
	if (students_list.empty())
	{
		cout << "���� ������ �����" << endl;
		system("pause");
		return;
	}
	int num;
	while (true)
	{
		cout << "������� ����� ���������� ������" << endl;
		int flag = scanf("%d", &num);
		fflush (stdin);
		if (!flag)
			continue;
		if ((num > students_list.size()) || (num < 1))
		{
			cout << "��� ������ � ����� �������" << endl;
			continue;
		}
		break;
	}
	student temp;
	while (true)
	{
		cout << "������� �������" << endl;
		SetConsoleCP(1251);
		scanf("%s", temp.familiya);
		SetConsoleCP(866);
		cout << "������� ���" << endl;
		SetConsoleCP(1251);
		scanf("%s", temp.name);
		SetConsoleCP(866);
		cout << "������� ����" << endl;
		int flag = scanf("%d", &temp.day);
		fflush (stdin);
		if (!flag)
			continue;
		cout << "������� �����" << endl;
		flag = scanf("%d", &temp.month);
		fflush (stdin);
		if (!flag)
			continue;
		cout << "������� ���" << endl;
		flag = scanf("%d", &temp.year);
		fflush (stdin);
		if (!flag)
			continue;
		cout << "������� ��������" << endl;
		SetConsoleCP(1251);
		scanf("%s", temp.institute);
		SetConsoleCP(866);
		cout << "������� ������" << endl;
		SetConsoleCP(1251);
		scanf("%s", temp.team);
		SetConsoleCP(866);
		cout << "������� ����" << endl;
		flag = scanf("%d", &temp.course);
		fflush (stdin);
		if (!flag)
			continue;
		cout << "������� �����" << endl;
		flag = scanf("%f", &temp.sr_ball);
		fflush (stdin);
		if (!flag)
			continue;
		break;
	}
	students_list[num - 1] = temp;
}



//��������� �������� �������� �� �������
void erase_element (vector<student>& students_list)
{
	if (students_list.empty())
	{
		cout << "���� ������ �����" << endl;
		system("pause");
		return;
	}
	int num;
	while (true)
	{
		cout << "������� ����� ��������� ������" << endl;
		int flag = scanf("%d", &num);
		fflush (stdin);
		if (!flag)
			continue;
		if ((num > students_list.size()) || (num < 1))
		{
			cout << "��� ������ � ����� �������" << endl;
			continue;
		}
		break;
	}
	vector<student>::iterator cur = students_list.begin();
	cur += num - 1;
	students_list.erase(cur);
}


int main()
{
	setlocale(LC_ALL, "Russian");
	int uprav;
	bool flag = true;
	vector<student> list = read();
	while (flag)
	{
		system("cls");
		show(list);
		cout << "������� 1 ��� ���������� ��������.\n������� 2 ��� ��������� ��������.\n������� 3 ��� �������� ��������.\n������� 4 ��� ���������� �� ��������.\n������� 5 ��� ���������� �� ���� ��������.\n������� 6 ��� ������ �� ���.\n������� 7 ��� ������ �� ���� ��������.\n������� 8, ����� �������� ���������� � ������� ������.\n������� 9, ����� ��������� ���������.\n������� 0 ��� ������." << endl;
		int OK = scanf("%d", &uprav);
		fflush (stdin);
		if (!OK)
			continue;
		switch (uprav)
		{
		case 1:
			{
				add_element(list);
				break;
			}
		case 2:
			{
				edit_element(list);
				break;
			}
		case 3:
			{
				erase_element(list);
				break;
			}
		case 4:
			{
				sort_fio(list);
				break;
			}
		case 5:
			{
				sort_birthday(list);
				break;
			}
		case 6:
			{
				find_fio(list);
				break;
			}
		case 7:
			{
				find_birthday(list);
				break;
			}
		case 8:
			{
				balli(list);
				break;
			}
		case 9:
			{
				write(list);
				break;
			}
		case 0:
			{
				flag = false;
				break;
			}
		}
	}
	return 0;
}